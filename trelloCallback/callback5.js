const boardInfo = require('./callback1');
const listInfo = require('./callback2');
const cardInfo = require('./callback3');

//callback for cardInfo function .list all card
const callback = (list) => {
  console.log(list);
};

//callback for listInfo function
const listCallBack = (thanosBoardList) => {
  console.log(thanosBoardList);
  let mindAndSpace = thanosBoardList.filter(
    (ele) => ele.name === 'Mind' || ele.name === 'Space'
  );
  let mindAndSpaceId = mindAndSpace.map((ele) => ele.id);

  let mindAndSpaceCard = [];
  let execution = 0;
  for (let index = 0; index < mindAndSpaceId.length; index++) {
    cardInfo(mindAndSpaceId[index], (err, result) => {
      if (err) {
        console.log(err);
        execution++;
      } else {
        mindAndSpaceCard.push(result);
        execution++;
      }
      if (execution == 2) {
        console.log(mindAndSpaceCard);
      }
    });
  }
};

//callback for boarInfo function
const boardCallBack = (thanosBoard) => {
  console.log(thanosBoard);
  let boardId = thanosBoard.id;
  //fetching list information
  listInfo(boardId, listCallBack);
};

// main function
const thanosInformationCardMindAndSpace = (boardId) => {
  setTimeout(() => {
    //fetching board information
    boardInfo(boardId, boardCallBack);
  }, 2 * 2000);
};


module.exports = thanosInformationCardMindAndSpace;
