/* 
	Problem 3: Write a function that will return all cards that belong to a particular list
     based on the listID that is passed to it from the given data in cards.json. 
     Then pass control back to the code that called it by using a callback function.
*/

const cardInfo = require('../data/trello-callbacks/cards.json');

const findcardsByListId = (listId, callback) => {
  console.log(`Fetching car information of list id ${listId}`);
  setTimeout(() => {
    if (cardInfo.hasOwnProperty(listId)) {
      callback(null,cardInfo[listId]);
    } else {
      callback(null,
        `Fetching car information of list id ${listId} is not available`
      );
    }
  }, 2 * 1000);
};

module.exports = findcardsByListId;
