/* 
	Problem 2: Write a function that will return all lists that belong to a board 
    based on the boardID that is passed to it from the given data in lists.json. 
    Then pass control back to the code that called it by using a callback function.
*/

const listinfo = require('../data/trello-callbacks/lists.json');

const listInformationByBoardId = (boardId, callback) => {
  console.log(`Fetching list of board id ${boardId}...`);
  setTimeout(() => {
    if (listinfo.hasOwnProperty(boardId)) {
      callback(listinfo[boardId]);
    } else {
      callback('List is not available for given board');
    }
  }, 2 * 1000);
};

module.exports = listInformationByBoardId;
