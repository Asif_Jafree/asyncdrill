/* 
	Problem 1: Write a function that will return a particular board's information 
    based on the boardID that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/
const boards = require('../data/trello-callbacks/boards.json');

function boardInfoById(boardId, callback) {
  console.log('Fetching board information ...');
  setTimeout(() => {
    let boardInfo = boards.find((board) => {
      if (board.id === boardId) return true;
    });
    if (boardInfo != undefined) {
      callback(boardInfo);
    } else {
      callback('No board found with this id');
    }
  }, 2 * 1000);
}

module.exports = boardInfoById;
