const boardInfo = require('./callback1');
const listInfo = require('./callback2');
const cardInfo = require('./callback3');

//callback for cardInfo function .list all card
const mindCallback = (list) => {
  console.log(list);
};

//callback for listInfo function
const listCallBack = (thanosBoardList) => {
  console.log(thanosBoardList);
  let mindList = thanosBoardList.find((ele) => ele.name === 'Mind');
  let mindId = mindList.id;
  //fetching mind card information
  cardInfo(mindId, mindCallback);
};

//callback for boarInfo function
const boardCallBack = (thanosBoard) => {
  console.log(thanosBoard);
  let boardId = thanosBoard.id;
  //fetching list information
  listInfo(boardId, listCallBack);
};

// main function
const thanosInformation = (boardId) => {
  setTimeout(() => {
    //fetching board information
    boardInfo(boardId, boardCallBack);
  }, 2 * 2000);
};

module.exports = thanosInformation;
