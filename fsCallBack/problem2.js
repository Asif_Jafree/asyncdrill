const fs = require('fs');
const rimraf = require('rimraf')
function readLipsumFile() {
  fs.readFile('data/fs-callbacks/lipsum.txt', 'utf-8', (err, data) => {
    if (err) {
      console.log(err);
    } else {
      convertDataInUpperCase(data);
    }
  });
}

function convertDataInUpperCase(data) {
  let lipsumData = data.toUpperCase();
  fs.writeFile('data/fs-callbacks/outputFolder/upperCase.txt', JSON.stringify(lipsumData), (err) => {
    if (err) {
      console.error(err);
    } else {
      sentenceWithLowerCaseFile('data/fs-callbacks/outputFolder/upperCase.txt');
    }
  });
}

function sentenceWithLowerCaseFile(path) {
  fs.readFile(path, 'utf-8', (err, data) => {
    if (err) {
      console.log(err);
    } else {
      let lowerCaseData = data.toLowerCase();
      let sentence = lowerCaseData.match(/[^\.!\?]+[\.!\?]+/g);
      let fileName = 'data/fs-callbacks/outputFolder/sentence.txt';
      fs.writeFile(fileName, JSON.stringify(sentence), () => {
        if (err) {
          console.error(err);
        } else {
          sortContentInNewFIle(fileName);
        }
      });
    }
  });
}

function sortContentInNewFIle(file) {
  fs.readFile(file, 'utf-8', (err, data) => {
    if (err) {
      console.error(err);
    } else {
      let sortedData = JSON.parse(data).sort();
      let filePath = 'data/fs-callbacks/outputFolder/sortedData.txt'
      fs.writeFile(filePath, JSON.stringify(sortedData), (err) => {
        if (err) {
          console.error(err);
        } else {
          readLastAndDeleteAllFile(filePath)
        }
      });
    }
  });
}

function readLastAndDeleteAllFile(path){
    fs.readFile(path,'utf-8',(err,data)=>{
        if(err){
            console.error(err);
        }
        else{
           // console.log(data);
            // fs.rm("data/fs-callbacks/outputFolder",{recursive:true},(err)=>{
            //   if(err){
            //     console.error(err);
            //   }
            //   else{
            //     console.log("Work Done");

            //   }
             
          //  })       
        }
    })
}


module.exports = readLipsumFile;


