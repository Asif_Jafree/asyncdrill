const remoteFunction = require('../problem1');
const createDirectory = remoteFunction.createDirectory;
const deleteAllFile = remoteFunction.deleteAllFile;
const createRandomFiles = remoteFunction.createRandomFiles;
const deleteDirectory = remoteFunction.deleteDirectory;
let path = 'folder';

const fs = require('fs');

function callback(err, result) {
  if (err) {
    console.log(err);
  } else {
    console.log('All files Created Sucessfully');
  }
}

createDirectory(path, (err, result) => {
  if (err) {
    console.error(err);
  } else {
    let execution = 0;
    for (let index = 1; index < 11; index++) {
      createRandomFiles(path, index, (err, result) => {
        if (err) {
          console.error(err);
          execution++;
        } else {
          execution++;
        }
        if (execution === 10) {
          callback(err, result);
          let deletion = 0;
          for (let index = 1; index < 11; index++) {
            let filePath = `folder/random${index}.json`;
            deleteAllFile(filePath, (err, result) => {
              if (err) {
                console.error(err);
                deletion++;
              } else {
                deletion++;
              }
              if (deletion == 10) {
                console.log('Delete all files sucessfully');
                deleteDirectory('folder', (err, result) => {
                  if (err) {
                    console.error(err);
                  } else {
                    console.log('Folder deleted successfully');
                  }
                });
              }
            });
          }
        }
      });
    }
  }
});
