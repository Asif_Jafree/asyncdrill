/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require('fs');
let path = 'folder';

function deleteAllFile(filePath, callback) {
  fs.unlink(filePath, (err, result) => {
    if (err) {
      callback(err);
    } else {
      callback(err, result);
    }
  });
}

function createDirectory(path, callback) {
  fs.mkdir(path, (err, result) => {
    if (err) {
      callback(err);
    } else {
      callback(null, result);
    }
  });
}

function createRandomFiles(path, index, callback) {
  console.log(path);
  fs.writeFile(
    `${path}/random${index}.json`,
    JSON.stringify('file no ' + index),
    (err, result) => {
      if (err) {
        callback(err);
      } else {
        callback(null, result);
      }
    }
  );
}


function deleteDirectory(folderName,callback) {
  fs.rmdir(folderName, (err,result) => {
    if (err) {
      callback(err)
    } else {
      callback(null,result)
    }
  });
}

module.exports = { createRandomFiles, createDirectory, deleteAllFile, deleteDirectory };
